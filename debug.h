#ifdef DBG
#define DP(fmt, ...) \
	    do { printf("my_file: " fmt, ## __VA_ARGS__); } while (0)
#else
#define DP(fmt, ...) \
	    do { } while (0)
#endif
