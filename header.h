#include <assert.h>
struct pkg_number{
	int type;
};

struct pkg_getattr_send{
	char path[300];
};

struct pkg_getattr_receive{
	int res;
	struct stat stbuf;
};

struct pkg_access_send{
	char path[300];
	int mask;
};

struct pkg_access_receive{
	int res;
};

struct pkg_readlink_send{
	char path[300];
	char buf[1000];
	size_t size;
};

struct pkg_readlink_receive{
	int res;
	char buf[1000];
};

struct pkg_readdir_send{
	char path[300];
};

struct pkg_readdir_receive{
	struct stat st;
	struct dirent de;
	int flag;
};

struct pkg_readdir2_send{
	int res;	
};

struct pkg_readdir_receive2{
	struct stat st;
};

struct pkg_mknod_send{
	char path[300];
	mode_t mode;
	dev_t rdev;
};

struct pkg_mknod_receive{
	int res;
};

struct pkg_mkdir_send{
	char path[300];
	mode_t mode;
};

struct pkg_mkdir_receive{
	int res;
};

struct pkg_unlink_send{
	char path[300];	
};

struct pkg_unlink_receive{
	int res;
};

struct pkg_rmdir_send{
	char path[300];
};

struct pkg_rmdir_receive{
	int res;
};

struct pkg_symlink_send{
	char from[300];
	char to[300];
};

struct pkg_symlink_receive{
	int res;
};

struct pkg_link_send{
	char from[300];
	char to[300];
};

struct pkg_link_receive{
	int res;
};

struct pkg_rename_send{
	char from[300];
	char to[300];
};

struct pkg_rename_receive{
	int res;
};

struct pkg_chmod_send{
	char path[300];
	mode_t mode;
};

struct pkg_chmod_receive{
	int res;
};

struct pkg_chown_send{
	char path[300];
	uid_t uid;
	gid_t gid;
};

struct pkg_chown_receive{
	int res;
};

struct pkg_open_send{
	char path[300];
	struct fuse_file_info fi;
};

struct pkg_open_receive{
	int res;
};

struct pkg_read_send{
	char path[300];
	//char buf[100000000];
	size_t size;
	off_t offset;
};

struct pkg_read_receive{
	int res;
	//char buf[100000000];
};

struct pkg_write_send{
	char path[300];
	char buf[10000];
	size_t size;
	off_t offset;
};

struct pkg_write_receive{
	int res;
	char buf[10000];
};

struct pkg_truncate_send{
	char path[300];
	off_t size;
};

struct pkg_truncate_receive{
	int res;
};

struct pkg_utimens_send{
	char path[300];
	struct timespec ts[2];
};

struct pkg_utimens_receive{
	int res;
};

struct pkg_statfs_send{
	char path[300];
	struct statvfs stbuf;
};

struct pkg_statfs_receive{
	int res;
};

struct pkg_fallocate_send{
	char path[300];
	off_t offset;
	off_t length;
};

struct pkg_fallocate_receive{
	int res;
};

#define PACK( STR1 , STR2 , STR3) STR1 ## STR2 ## STR3

#define SEND_SYSCALL( T ) \
	void send_syscall_##T(struct pkg_##T *obj) { \
	if (( numbytes = writeAll( sockfd1 , (const void *)obj , sizeof(struct pkg_##T)))== -1) { \
					perror("talker: sendto"); \
					exit(1); \
				} \
		assert( numbytes == sizeof(struct pkg_##T));\
	} \

#define RECEIVE_SYSCALL( T ) \
	void receive_syscall_##T(struct pkg_##T *obj) { \
		if ((numbytes = readAll(sockfd1, obj,sizeof(struct pkg_##T))) == -1) { \
		perror("recvfrom"); \
		exit(1); \
	} \
		assert( numbytes == sizeof(struct pkg_##T));\
	} \

#define INIT( T ) \
	struct T * init_##T() { \
	struct T * obj  = ( struct T *)malloc( sizeof (struct T )); \
		return obj; \
	} \

#define XMP_PATH(NAME , NUMBER ) \
	static int xmp_##NAME(const char *path){ \
		int res; \
		char fullpath[PATH_MAX]; \
		create_fullpath(fullpath, path); \
		DP("called %s. Path: %s\n",__func__,fullpath);\
		struct pkg_number *obj1 = init_pkg_number(); \
		obj1 -> type = NUMBER; \
		send_syscall_number( obj1); \
		struct PACK(pkg_ , NAME , _send) *obj2 = PACK(init_pkg_ , NAME , _send ) (); \
		strcpy(obj2->path,fullpath); \
		PACK( send_syscall_ , NAME , _send) (obj2); \
		DP("sent to server\n"); \
		struct PACK(pkg_ , NAME , _receive) *obj3 = PACK (init_pkg_ , NAME , _receive) (); \
		PACK( receive_syscall_ , NAME , _receive) (obj3); \
		DP("received from server\n"); \
		res = obj3->res; \
		free(obj1); free(obj2); free(obj3); \
		if (res < 0){ \
			DP("%s failed\n\n\n",__func__); \
			return res; \
		}\
		DP("%s done\n\n\n",__func__);\
		return 0; \
	} \

#define XMP_FROMTO( NAME , NUMBER ) \
	static int xmp_##NAME(const char *from, const char *to)\
	{\
	int res;\
	char fullpath1[PATH_MAX], fullpath2[PATH_MAX];\
	create_fullpath2(fullpath1, to, from);\
	create_fullpath(fullpath2, to);\
	DP("called %s. Path: %s %s\n",__func__,fullpath1, fullpath2);\
	struct pkg_number *obj1 = init_pkg_number();\
	obj1 -> type = NUMBER;\
	send_syscall_number( obj1);\
	struct PACK (pkg_ , NAME, _send) *obj2 = PACK(init_pkg_ , NAME , _send) ();\
	strcpy(obj2->from,fullpath1);\
	strcpy(obj2->to,fullpath2);\
	PACK (send_syscall_ , NAME , _send) (obj2);\
		DP("sent to server\n"); \
	struct PACK (pkg_ ,NAME ,_receive) *obj3 = PACK( init_pkg_ , NAME , _receive) ();\
	PACK (receive_syscall_ , NAME , _receive)(obj3);\
		DP("received from server\n"); \
	res = obj3->res;\
	free(obj1); free(obj2); free(obj3);\
	if (res < 0){\
		DP("%s failed\n\n\n",__func__); \
		return res;\
	}\
	DP("%s done\n\n\n",__func__);\
	return 0;\
}\

#define XMP_MODE( NAME, NUMBER )\
	static int xmp_##NAME(const char *path, mode_t mode)\
	{\
	int res;\
	char fullpath[PATH_MAX];\
	create_fullpath(fullpath, path);\
	DP("called %s. Path: %s\n",__func__,fullpath);\
	struct pkg_number *obj1 = init_pkg_number();\
	obj1 -> type = NUMBER ;\
	send_syscall_number( obj1);\
	struct PACK (pkg_ , NAME , _send) *obj2 = PACK ( init_pkg_ , NAME ,  _send) ();\
	strcpy(obj2->path,fullpath);\
	obj2->mode = mode;\
	PACK (send_syscall_ , NAME , _send)(obj2);\
	DP("sent to server\n"); \
	struct PACK (pkg_ , NAME , _receive) *obj3 = PACK (init_pkg_ , NAME , _receive)();\
	PACK ( receive_syscall_ , NAME , _receive)(obj3);\
	DP("received from server\n"); \
	res = obj3->res;\
	free(obj1); free(obj2); free(obj3);\
	if (res < 0){\
		DP("%s failed\n\n\n",__func__); \
		return res;\
	}\
	DP("%s done\n\n\n",__func__); \
	return 0;\
}\

#define CALL( V , NUMBER ) \
	do{ \
	printf("call captured, receiving pkg \n"); \
	struct PACK(pkg_ , V , _send) *obj ##NUMBER = PACK(init_pkg_ , V , _send) (); \
	PACK( receive_syscall_ , V , _send) (obj ##NUMBER); \
	printf("calling handle " #V "\n" ); \
	handle_ ##V(obj ##NUMBER); \
	free(obj ##NUMBER); } \
	while(0);


#ifdef DEBUG
#define DPRINTF(fmt, ...) \
	    do { printf("my_file: " fmt, ## __VA_ARGS__); } while (0)
#else
#define DPRINTF(fmt, ...) \
	    do { } while (0)
#endif

INIT ( pkg_number);

INIT ( pkg_access_send );
INIT ( pkg_access_receive );

INIT ( pkg_getattr_send );
INIT ( pkg_getattr_receive );

INIT ( pkg_readlink_send );
INIT ( pkg_readlink_receive);

INIT ( pkg_readdir_send );
INIT ( pkg_readdir2_send );
INIT ( pkg_readdir_receive );

INIT ( pkg_mknod_send );
INIT ( pkg_mknod_receive );

INIT ( pkg_mkdir_send );
INIT ( pkg_mkdir_receive );

INIT ( pkg_unlink_send );
INIT ( pkg_unlink_receive );

INIT ( pkg_rmdir_send );
INIT ( pkg_rmdir_receive);

INIT ( pkg_symlink_send );
INIT ( pkg_symlink_receive );

INIT ( pkg_rename_send );
INIT ( pkg_rename_receive );

INIT ( pkg_link_send);
INIT ( pkg_link_receive);

INIT ( pkg_chmod_send);
INIT ( pkg_chmod_receive);

INIT ( pkg_chown_send);
INIT ( pkg_chown_receive);

INIT ( pkg_open_send);
INIT ( pkg_open_receive);

INIT ( pkg_read_send);
INIT ( pkg_read_receive);

INIT ( pkg_write_send);
INIT ( pkg_write_receive);

INIT ( pkg_truncate_send);
INIT ( pkg_truncate_receive);

INIT ( pkg_utimens_send);
INIT ( pkg_utimens_receive);

INIT ( pkg_statfs_send);
INIT ( pkg_statfs_receive);

INIT ( pkg_fallocate_send);
INIT ( pkg_fallocate_receive);
