/*
  FUSE: Filesystem in Userspace
  Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>
  Copyright (C) 2011       Sebastian Pipping <sebastian@pipping.org>

  This program can be distributed under the terms of the GNU GPL.
  See the file COPYING.

  gcc -Wall fusexmp.c `pkg-config fuse --cflags --libs` -o fusexmp
*/

#define SERVER
#define FUSE_USE_VERSION 26

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef linux
/* For pread()/pwrite()/utimensat() */
#define _XOPEN_SOURCE 700
#endif

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#ifdef HAVE_SETXATTR
#include <sys/xattr.h>
#endif

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include "header.h"
#include "mysocket.c"

//#define MNTDIR "/home/shehbaz/hg2"
#define MNTDIR "/home/aswin/docs/IITD"
#define PATH_MAX 300

#define MYPORT 3490    // the port users will be connecting to

#define BACKLOG 10     // how many pending connections queue will hold

int numbytes, sockfd1, sockfd2;
socklen_t addr_len;

SEND_SYSCALL ( getattr_receive);
SEND_SYSCALL ( access_receive);
SEND_SYSCALL ( readlink_receive);
SEND_SYSCALL ( mknod_receive);
SEND_SYSCALL ( mkdir_receive);
SEND_SYSCALL ( unlink_receive);
SEND_SYSCALL ( rmdir_receive);
SEND_SYSCALL ( symlink_receive);
SEND_SYSCALL ( rename_receive);
SEND_SYSCALL ( link_receive);
SEND_SYSCALL ( chmod_receive); 
SEND_SYSCALL ( chown_receive);
SEND_SYSCALL ( open_receive);
SEND_SYSCALL ( read_receive);
SEND_SYSCALL ( write_receive);
SEND_SYSCALL ( readdir_receive);
SEND_SYSCALL ( truncate_receive);
SEND_SYSCALL ( utimens_receive);
SEND_SYSCALL ( statfs_receive); 
SEND_SYSCALL ( fallocate_receive);

RECEIVE_SYSCALL( number);
RECEIVE_SYSCALL( getattr_send);
RECEIVE_SYSCALL( access_send);
RECEIVE_SYSCALL( readlink_send);
RECEIVE_SYSCALL( readdir_send);
RECEIVE_SYSCALL( mknod_send);
RECEIVE_SYSCALL( mkdir_send);
RECEIVE_SYSCALL( unlink_send);
RECEIVE_SYSCALL( rmdir_send);
RECEIVE_SYSCALL( symlink_send);
RECEIVE_SYSCALL( rename_send);
RECEIVE_SYSCALL( link_send);
RECEIVE_SYSCALL( chmod_send);
RECEIVE_SYSCALL( chown_send);
RECEIVE_SYSCALL( open_send);
RECEIVE_SYSCALL( read_send);
RECEIVE_SYSCALL( write_send);
RECEIVE_SYSCALL( truncate_send);
RECEIVE_SYSCALL( utimens_send);
RECEIVE_SYSCALL( statfs_send);
RECEIVE_SYSCALL( fallocate_send);

void sigchld_handler(int s)
{
    while(wait(NULL) > 0);
}

static void create_fullpath(char fpath[PATH_MAX], const char *path)
{
	strcpy(fpath, MNTDIR);
    strncat(fpath, path, PATH_MAX); 
}

void handle_getattr(struct pkg_getattr_send *obj1){
	struct pkg_getattr_receive *obj2 =  init_pkg_getattr_receive(); 
	int res;
	memset(&obj2->stbuf, 0, sizeof(struct stat));
	res = lstat(obj1->path,&obj2->stbuf);
	if(res==-1) obj2 -> res = -errno;
	else obj2->res = res;
	printf("path = %s\n", obj1->path);
	printf("sending getattr result %d\n",obj2->res);
	send_syscall_getattr_receive( obj2);
	printf("sent getattr result\n");
	free(obj2);
}

void handle_readdir(struct pkg_readdir_send *obj1){

	struct pkg_readdir_receive *obj2 =  init_pkg_readdir_receive(); 
	DIR *dp;
	struct dirent *de;

	printf("calling %s, path is %s\n", __func__, obj1->path);

	dp = opendir(obj1->path);
	if (dp == NULL){
		obj2->flag = -errno;
		send_syscall_readdir_receive(obj2);
		printf("dir no exist, obj2 flag = %d\n", obj2->flag);
		return;
		//return -errno;
	}

	while ( (de = readdir(dp)) != NULL) {

		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;

		obj2->de = *de;
		strcpy(obj2->de.d_name, de->d_name); // be on the safe side
		obj2->st = st;
		obj2->flag = 1;

		send_syscall_readdir_receive(obj2);

	}
	obj2->flag = 0;
	printf("readdir done\n");
	send_syscall_readdir_receive(obj2);

	closedir(dp);


}

void handle_access(struct pkg_access_send *obj1)
{
	int res;
	printf("%s(): %s\n",__func__,obj1->path);	
	struct pkg_access_receive *obj2 = init_pkg_access_receive();
	res = access(obj1->path,obj1->mask);
	if(res == -1) { obj2->res = -errno;}
	else {obj2->res = 0;}
	printf("sending access result\n");
	send_syscall_access_receive(obj2);
	printf("sent access result\n");
	free(obj2);
}

void handle_readlink(struct pkg_readlink_send *obj1)
{
	int res;
	printf("%s(): %s\n",__func__,obj1->path);	
	struct pkg_readlink_receive *obj2 = init_pkg_readlink_receive();
	res = readlink(obj1->path,obj1->buf,obj1->size -1);
	if(res == -1) { obj2->res = -errno;}
	else {obj2->res = 0;}
	printf("sending readlink result\n");
	send_syscall_readlink_receive(obj2);
	printf("sent readlink result\n");
	free(obj2);
}

void handle_mknod(struct pkg_mknod_send *obj1)
{
	int res;
	/* On Linux this could just be 'mknod(path, mode, rdev)' but this
	   is more portable */
	if (S_ISREG(obj1->mode)) {
		res = open(obj1->path, O_CREAT | O_EXCL | O_WRONLY, obj1->mode);
		if (res >= 0)
			res = close(res);
	} else if (S_ISFIFO(obj1->mode))
		res = mkfifo(obj1->path, obj1->mode);
	else
		res = mknod(obj1->path, obj1->mode, obj1->rdev);

	struct pkg_mknod_receive *obj2 = init_pkg_mknod_receive();
	if(res == -1) { obj2->res = -errno;}
	else { obj2->res = res;}
	printf("sending mknod result\n");
	send_syscall_mknod_receive(obj2);
	printf("sent readlink result\n");
	free(obj2);
}

static int handle_mkdir(struct pkg_mkdir_send *obj1)
{
	int res;

	res = mkdir(obj1->path, obj1->mode);
	struct pkg_mkdir_receive *obj2 = init_pkg_mkdir_receive();
	if(res == -1) { obj2->res = -errno;}
	else { obj2->res = res;}
	printf("sending mkdir result\n");
	send_syscall_mkdir_receive(obj2);
	printf("sent mkdir result\n");
	free(obj2);
}

static int handle_unlink(struct pkg_unlink_send *obj1)
{
	int res;

	res = unlink(obj1->path);
	struct pkg_unlink_receive *obj2 = init_pkg_unlink_receive();
	if(res == -1) { obj2->res = -errno;}
	else { obj2->res = res;}
	printf("sending unlink result\n");
	send_syscall_unlink_receive(obj2);
	printf("sent unlink result\n");
	free(obj2);
}

void handle_rmdir(struct pkg_rmdir_send *obj1)
{
	int res;
	res = rmdir(obj1->path);
	struct pkg_rmdir_receive *obj2 = init_pkg_rmdir_receive();
	if(res == -1) { obj2->res = -errno;}
	else { obj2->res = res;}
	printf("sending rmdir result\n");
	send_syscall_rmdir_receive(obj2);
	printf("sent rmdir result\n");
	free(obj2);
}

void handle_symlink(struct pkg_symlink_send *obj1)
{
	int res;
	res = symlink(obj1->from, obj1->to);
	struct pkg_symlink_receive *obj2 = init_pkg_symlink_receive();
	if(res == -1) { obj2->res = -errno;}
	else { obj2->res = res;}
	printf("sending symlink result\n");
	send_syscall_symlink_receive(obj2);
	printf("sent symlink result\n");
	free(obj2);
}

void handle_rename(struct pkg_rename_send *obj1)
{
	int res;
	res = rename(obj1->from, obj1->to);
	struct pkg_rename_receive *obj2 = init_pkg_rename_receive();
	if(res == -1) { obj2->res = -errno;}
	else { obj2->res = res;}
	printf("sending rename result\n");
	send_syscall_rename_receive(obj2);
	printf("sent rename result\n");
	free(obj2);
}

void handle_link(struct pkg_link_send *obj1)
{
	int res;
	res = link(obj1->from, obj1->to);
	struct pkg_link_receive *obj2 = init_pkg_link_receive();
	if(res == -1) { obj2->res = -errno;}
	else { obj2->res = res;}
	printf("sending link result\n");
	send_syscall_link_receive(obj2);
	printf("sent link result\n");
	free(obj2);
}

void handle_chmod(struct pkg_chmod_send *obj1)
{
	int res;
	res = chmod(obj1->path, obj1->mode);
	struct pkg_chmod_receive *obj2 = init_pkg_chmod_receive();
	if(res == -1) { obj2->res = -errno;}
	else { obj2->res = res;}
	printf("sending chmod result\n");
	send_syscall_chmod_receive(obj2);
	printf("sent chmod result\n");
	free(obj2);
}

void handle_chown(struct pkg_chown_send *obj1)
{
	int res;
	res = lchown(obj1->path, obj1->uid, obj1->gid);
	struct pkg_chown_receive *obj2 = init_pkg_chown_receive();
	if(res == -1) { obj2->res = -errno;}
	else { obj2->res = res;}
	printf("sending chown result\n");
	send_syscall_chown_receive(obj2);
	printf("sendt chown result\n");
	free(obj2);
}

void handle_open(struct pkg_open_send *obj1)
{
	int res;
	res = open(obj1->path, obj1->fi.flags);
	struct pkg_open_receive *obj2 = init_pkg_open_receive();
	if(res == -1) { 
		obj2->res = -errno;
	}else { 
		obj2->res = res;
		close(res);
	}
	printf("sending open result\n");
	send_syscall_open_receive(obj2);
	free(obj2);
	printf("sendt open result\n");
}

void handle_read(struct pkg_read_send *obj1)
{
	int fd;
	int res;
	char *buffer = (char *)malloc(obj1->size);

	printf("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX%ld\n",obj1->size);
	struct pkg_read_receive *obj2 = init_pkg_read_receive();
	fd = open(obj1->path, O_RDONLY);
	if (fd == -1){
		obj2->res = -errno;
		send_syscall_read_receive(obj2);
		free(obj2);
		return;
	}
	res = pread(fd, buffer, obj1->size, obj1->offset);
	if (res == -1){
		obj2->res = -errno;
		send_syscall_read_receive(obj2);
		free(obj2);
		return ;
	}
	close(fd);
	obj2->res = res;
	send_syscall_read_receive(obj2);
	//disregarding convention here
	writeAll(sockfd1, buffer, obj1->size);
	free(obj2);
	return;
}

void handle_write(struct pkg_write_send *obj1)
{
	int fd;
	int res;

	struct pkg_write_receive *obj2 = init_pkg_write_receive();
	fd = open(obj1->path, O_WRONLY);
	if (fd == -1){
		obj2->res = -errno;
		send_syscall_write_receive(obj2);
		free(obj2);
		return;
	}
	res = pwrite(fd, obj1->buf, obj1->size, obj1->offset);
	if (res == -1){
		obj2->res = -errno;
		send_syscall_write_receive(obj2);
		close(fd);
		free(obj2);
		return;
	}
	close(fd);
	obj2->res = res;
	send_syscall_write_receive(obj2);
	free(obj2);
	return;
}

void handle_truncate(struct pkg_truncate_send *obj1)
{
	int res;
	struct pkg_truncate_receive *obj2 = init_pkg_truncate_receive();
	res = truncate(obj1->path, obj1->size);
	if (res == -1){ obj2->res = -errno;}
	else { obj2->res = 0; }
	send_syscall_truncate_receive(obj2);
	free(obj2);
	return;
}

//#ifdef HAVE_UTIMENSAT
void handle_utimens(struct pkg_utimens_send *obj1)
{
	int res;
	struct pkg_utimens_receive *obj2 = init_pkg_utimens_receive();

	/* don't use utime/utimes since they follow symlinks */
	res = utimensat(0, obj1->path, obj1->ts, AT_SYMLINK_NOFOLLOW);
	if (res == -1) { obj2->res = -errno;}
	else { obj2->res = 0;}
	send_syscall_utimens_receive(obj2);
	free(obj2);
	return;
}
//#endif


void handle_statfs(struct pkg_statfs_send *obj1)
{
	int res;
	struct pkg_statfs_receive *obj2 = init_pkg_statfs_receive();
	printf("handling statvfs\n");
	printf("path sent to statvfs server = %s\n", obj1->path);
	res = statvfs(obj1->path, &obj1->stbuf);
	printf("statvfs returned %d\n", res);
	if (res == -1) { obj2->res = -errno;}
	else { obj2->res = 0;}
	send_syscall_statfs_receive(obj2);
	free(obj2);
	return;
}

#ifdef HAVE_POSIX_FALLOCATE
void handle_fallocate(struct pkg_fallocate_send *obj1)
{
	int fd;
	int res;
	struct pkg_fallocate_receive *obj2 = init_pkg_fallocate_receive();
	fd = open(obj1->path, O_WRONLY);
	if (fd == -1){
		obj2->res = -errno;
	}else{
		res = -posix_fallocate(fd, obj1->offset, obj1->length);
		close(fd);
		obj2->res = res;
	}
	send_syscall_fallocate_receive(obj2);
	free(obj2);
	return;
}
#endif

int main(int argc, char *argv[])
{

	/* 1. Create a socket and connect to it.
	 * 2. Start listening for new requests.
	 * 3. Check type of request that has been received from the client
	 * 4. Use switch case to determine the kind of file system request. Once that is sorted call the appropriate function.
	 *  function call will contain all values that are received.
	 *  5. In each of the sub children functions, the child MARSHALLs all the results into a struct, and calls send. 
	 *  6. value is received by client and UNMARSHALLING is done on client sides sub-functions.
	 *
	 * */
	
	struct pkg_number *syscall_no;
	addr_len = sizeof (struct sockaddr_storage );
	 
	sockfd1 = startServer(5000);

	while(1){
		syscall_no = init_pkg_number();
		printf("listener: waiting to recvfrom...\n");
		receive_syscall_number (syscall_no);		
		printf("received request for :: %d\n",syscall_no->type);
		switch(syscall_no->type){
			case 1:
					CALL( getattr, 1);
					break;
			case 2:
					CALL( access, 2);
					break;

			case 3:
					CALL( readlink, 3);
					break;

			case 4:
					CALL( readdir, 4);
					break;

			case 5:
					CALL( mknod, 5);
					break;
			case 6:
					CALL( mkdir, 6);
					break;
			case 7:
					CALL( unlink, 7);
					break;

			case 8:
					CALL( rmdir, 8);
					break;

			case 9: 
					CALL( symlink, 9);
					break;

			case 10:
					CALL( rename, 10);
					break;

			case 11:
					CALL( link, 11);
					break;

			case 12:
					CALL( chmod, 12);
					break;

			case 13:
					CALL( chown, 13);
					break;

			case 14:
					CALL( open, 14);
					break;

			case 15:
					CALL( read, 15);
					break;
		
			case 16:
					CALL( write, 16);
					break;

			case 17:
					CALL( truncate,17);
					break;

//#ifdef HAVE_UTIMENSAT
			case 18:
					CALL( utimens, 18);
					break;
//#endif

			case 19: 
					CALL( statfs, 19);
					break;

#ifdef HAVE_POSIX_FALLOCATE
			case 20:
					CALL( fallocate, 20);
					break;
#endif
			default:
					printf("nothing to send\n halting\n");
		
		}
		free(syscall_no);
	}
	close(sockfd1);
	return 0;
}
