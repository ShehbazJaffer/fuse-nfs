all: 	client server 

client: fusexmp_client.c mysocket.c header.h
	gcc fusexmp_client.c -lfuse -D_FILE_OFFSET_BITS=64 -o client

client2: fusexmp_client.c mysocket.c header.h
	gcc fusexmp_client.c -lfuse -D_FILE_OFFSET_BITS=64 -o client2 -E

server: fusexmp_server.c mysocket.c header.h
	gcc fusexmp_server.c -lfuse -D_FILE_OFFSET_BITS=64 -o server

server2: fusexmp_server.c mysocket.c
	gcc fusexmp_server.c -lfuse -D_FILE_OFFSET_BITS=64 -o server2 -E

clean: 
	rm -rf client server client2 server2
