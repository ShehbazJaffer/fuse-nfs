#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
//#include <error.h>
#define MAXBUFLEN 100

#ifdef SERVER
#define READPORT "4950" // the port users will be connecting to
#define WRITEPORT "4951"
#endif

#ifdef CLIENT
#define READPORT "4951" // the port users will be connecting to
#define WRITEPORT "4950"
#endif

#define h_addr h_addr_list[0]

// get sockaddr, IPv4 or IPv6:

struct sockaddr_storage their_addr; // for listener
struct addrinfo *p,*servinfo; // for talker

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int writeAll(int sockfd, const void *buffer, int size){
	int n;
	int temp = size;

	do{
		n = write(sockfd, buffer, size);
		if (n == -1) {
			return n;
		}
		size -= n;
		buffer += n;
	}while(size > 0);
	return temp;
}

int readAll(int sockfd,  void *buffer, int size){
	int n;

	int temp = size;
	do{
		n = read(sockfd, buffer, size);
		if (n == -1) {
			return n;
		}
		size -= n;
		buffer += n;
	}while(size > 0);
	return temp;

}

int startServer(int portno){

     struct sockaddr_in serv_addr, cli_addr;
     int n, sockfd, newsockfd;
     socklen_t clilen;
     sockfd = socket(AF_INET, SOCK_STREAM, 0); 
     if (sockfd < 0)  
        error("ERROR opening socket");
     memset( &serv_addr, 0 ,sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0)  
              error( "ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     newsockfd = accept(sockfd, 
                 (struct sockaddr *) &cli_addr, 
                 &clilen);
     if (newsockfd < 0)  
          error("ERROR on accept");                                                                                                                   

     return newsockfd;

}

int 
startClient(int portno, char *serverip){
	
	int sockfd;
    struct sockaddr_in serv_addr;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    //if (server == NULL) {
    //    fprintf(stderr,"ERROR, no such host\n");
    //    exit(0);
    //}
//    memset(&serv_addr,  0 ,  sizeof(serv_addr));
//    serv_addr.sin_family = AF_INET;
//    memcpy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr,server->h_length);
//    serv_addr.sin_port = htons(portno);
//
	memset(&serv_addr, '0', sizeof(serv_addr)); 

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno); 

    if(inet_pton(AF_INET, serverip, &serv_addr.sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
       exit(-1);
    } 

    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       printf("\n Error : Connect Failed \n");
       exit(-1);
    } 

	printf("connect done;\n");



    //if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
    //    error("ERROR connecting");

    return sockfd;

}
