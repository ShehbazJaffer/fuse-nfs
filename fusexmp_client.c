/*
  FUSE: Filesystem in Userspace
  Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>
  Copyright (C) 2011       Sebastian Pipping <sebastian@pipping.org>

  This program can be distributed under the terms of the GNU GPL.
  See the file COPYING.

  gcc -Wall fusexmp.c `pkg-config fuse --cflags --libs` -o fusexmp
*/

#define FUSE_USE_VERSION 26
#define CLIENT
#define DEBUG

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef linux
/* For pread()/pwrite()/utimensat() */
#define _XOPEN_SOURCE 700
#endif

#define DBG
#include "debug.h"

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include "header.h"
#include "mysocket.c"

#ifdef HAVE_SETXATTR
#include <sys/xattr.h>
#endif

#define h_addr h_addr_list[0]

#define PORT 3490 // the port client will be connecting to 

#define MAXDATASIZE 100 // max number of bytes we can get at once 


//#define MNTDIR "/home/shehbaz/hg2"
#define MNTDIR "/home/aswin/ds"
#define PATH_MAX 300

int sockfd1,sockfd2;
int numbytes;
	
int addr_len = sizeof (struct sockaddr_storage );

SEND_SYSCALL( number );
SEND_SYSCALL( getattr_send);
SEND_SYSCALL( access_send);
SEND_SYSCALL( readlink_send);
SEND_SYSCALL( readdir_send);
SEND_SYSCALL( mknod_send);
SEND_SYSCALL( mkdir_send);
SEND_SYSCALL( unlink_send);
SEND_SYSCALL( rmdir_send);
SEND_SYSCALL( symlink_send);
SEND_SYSCALL( rename_send );
SEND_SYSCALL( link_send);
SEND_SYSCALL( chmod_send);
SEND_SYSCALL( chown_send);
SEND_SYSCALL( open_send);
SEND_SYSCALL( read_send);
SEND_SYSCALL( write_send);
SEND_SYSCALL( truncate_send);
SEND_SYSCALL( utimens_send);
SEND_SYSCALL( statfs_send);
SEND_SYSCALL( fallocate_send);

RECEIVE_SYSCALL(getattr_receive);
RECEIVE_SYSCALL(access_receive);
RECEIVE_SYSCALL(readlink_receive);
RECEIVE_SYSCALL(readdir_receive);
//RECEIVE_SYSCALL(readdir_receive2);
RECEIVE_SYSCALL(mknod_receive);
RECEIVE_SYSCALL(mkdir_receive);
RECEIVE_SYSCALL(unlink_receive);
RECEIVE_SYSCALL(rmdir_receive);
RECEIVE_SYSCALL(symlink_receive);
RECEIVE_SYSCALL(rename_receive);
RECEIVE_SYSCALL(link_receive);
RECEIVE_SYSCALL(chmod_receive);
RECEIVE_SYSCALL(chown_receive);
RECEIVE_SYSCALL(open_receive);
RECEIVE_SYSCALL(read_receive);
RECEIVE_SYSCALL(write_receive);
RECEIVE_SYSCALL(truncate_receive);
RECEIVE_SYSCALL(utimens_receive);
RECEIVE_SYSCALL(statfs_receive);
RECEIVE_SYSCALL(fallocate_receive);

static void create_fullpath(char fpath[PATH_MAX], const char *path)
{
	strcpy(fpath, MNTDIR);
    strncat(fpath, path, PATH_MAX); 
}

static void create_fullpath2(char fpath[PATH_MAX], const char *loc, const char *file)
{
	int i;
	strcpy(fpath, MNTDIR);
	strcat(fpath, "/");
	for (i = strlen(loc); loc[i] != '/' && i>0; i--);
	strncpy(fpath,loc,i);
    strncat(fpath, file, PATH_MAX); 
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
	int res;
	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);

	DP("called %s. Path: %s\n",__func__,fullpath);	
	
	struct pkg_number *obj1 = init_pkg_number();
	obj1 -> type = 1;

	send_syscall_number( obj1);
	
	struct pkg_getattr_send *obj2 = init_pkg_getattr_send();
	strcpy(obj2->path,fullpath);
	send_syscall_getattr_send(obj2);
	
	DP("sent to server\n");
	struct pkg_getattr_receive *obj3 = init_pkg_getattr_receive();
	receive_syscall_getattr_receive(obj3);
	DP("%s - received from server\n",__func__);

	res = obj3->res;
	if( res < 0 ){
		free(obj1); 
		free(obj2); 
		free(obj3);
		DP("%s failed\n\n\n",__func__);	
		return res;
	}
	//printf("path = %s res = %d \n", fullpath, res);
	memset(stbuf, 0, sizeof(struct stat));
    stbuf->st_mode   =  obj3->stbuf.st_mode;
    stbuf->st_uid    =  obj3->stbuf.st_uid;
    stbuf->st_gid    =  obj3->stbuf.st_gid;
    stbuf->st_size   =  obj3->stbuf.st_size;
    stbuf->st_mtime  =  obj3->stbuf.st_mtime;
    stbuf->st_atime  =  obj3->stbuf.st_atime;
    stbuf->st_ctime  =  obj3->stbuf.st_ctime;

	free(obj1); free(obj2); free(obj3);
	DP("%s done\n\n\n",__func__);	
	return 0;
}

static int xmp_access(const char *path, int mask)
{
	int res;

	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);
	DP("called %s. Path: %s\n",__func__,fullpath);	

	struct pkg_number *obj1 = init_pkg_number();
	obj1 -> type = 2;
	send_syscall_number( obj1);
	
	struct pkg_access_send *obj2 = init_pkg_access_send();
	strcpy(obj2->path,fullpath);
	obj2->mask = mask;
	send_syscall_access_send(obj2);
	DP("sent to server\n");
	
	struct pkg_access_receive *obj3 = init_pkg_access_receive();
	receive_syscall_access_receive(obj3);
	DP("received from server\n");
	res = obj3 -> res;
	free(obj1); free(obj2); free(obj3);
	if (res < 0){
		DP("%s failed\n\n\n",__func__);	
		return res;
	}
	DP("%s done\n\n\n",__func__);	
	return 0;
}

static int xmp_readlink(const char *path, char *buf, size_t size)
{
	int res;
	//printf("%s(): %s\n",__func__,path);	

	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);
	DP("called %s. Path: %s\n",__func__,fullpath);	

	struct pkg_number *obj1 = init_pkg_number();
	obj1 -> type = 3;
	send_syscall_number( obj1);

	struct pkg_readlink_send *obj2 = init_pkg_readlink_send();
	strcpy(obj2->path,fullpath);
	strcpy(obj2->buf, buf);
	obj2->size = size;
	send_syscall_readlink_send(obj2);
	DP("sent to server\n");
	//res = readlink(fullpath, buf, size - 1);
	struct pkg_readlink_receive *obj3 = init_pkg_readlink_receive();
	receive_syscall_readlink_receive(obj3);
	DP("received from server\n");
	res = obj3->res;
	
	if (res < 0){
		free(obj1); free(obj2); free(obj3);
		DP("%s failed\n\n\n",__func__);	
		return res;
	}
	strncpy(buf,obj3->buf,res);
	
	buf[res] = '\0';
	free(obj1); free(obj2); free(obj3);
	DP("%s done\n\n\n",__func__);	
	return 0;
}


static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
		       off_t offset, struct fuse_file_info *fi)
{

	//printf("%s(): %s\n",__func__,path);	

	struct pkg_number *obj1 = init_pkg_number();
	struct pkg_readdir_send *obj2 =  init_pkg_readdir_send(); 
	struct pkg_readdir_receive *obj3 =  init_pkg_readdir_receive(); 
	int fill_ret;

	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);
	DP("called %s. Path: %s\n",__func__,fullpath);	

	(void) offset;
	(void) fi;


	obj1 -> type = 4;
	send_syscall_number( obj1);

	strcpy( obj2->path, fullpath);
	send_syscall_readdir_send(obj2);
	DP("path sent to server\n");

	while(1){

		receive_syscall_readdir_receive(obj3);
		DP("%s - received from server\n",__func__);
		if(obj3 -> flag < 0){
			return obj3 -> flag;
		}
		if( obj3 -> flag == 0) break;

		fill_ret = filler(buf, obj3->de.d_name, &(obj3->st), 0);
			//break;
		printf("filler returned%d\n", fill_ret);

	}
	DP("%s done\n\n\n",__func__);	

	return 0;
}



static int xmp_mknod(const char *path, mode_t mode, dev_t rdev)
{
	int res;
	
	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);
	DP("called %s. Path: %s\n",__func__,fullpath);	
	/* On Linux this could just be 'mknod(path, mode, rdev)' but this
	   is more portable */
	struct pkg_number *obj1 = init_pkg_number();
	obj1 -> type = 5;
	send_syscall_number( obj1);
	
	struct pkg_mknod_send *obj2 = init_pkg_mknod_send();
	strcpy(obj2->path,fullpath);
	obj2->mode = mode;
	obj2->rdev = rdev;
	send_syscall_mknod_send(obj2);

	DP("sent to server\n");

	struct pkg_mknod_receive *obj3 = init_pkg_mknod_receive();
	receive_syscall_mknod_receive(obj3);

	DP("received from server\n");
	res = obj3->res;
	free(obj1); free(obj2); free(obj3);
	if (res < 0){
		DP("%s failed\n\n\n",__func__);	
		return res;
	}
	DP("%s done\n\n\n",__func__);	
	return 0;
}

XMP_MODE( mkdir , 6);
XMP_PATH( unlink , 7 );
XMP_PATH( rmdir , 8);
XMP_FROMTO( symlink , 9);
XMP_FROMTO( rename , 10);
XMP_FROMTO( link, 11);
XMP_MODE( chmod , 12);

static int xmp_chown(const char *path, uid_t uid, gid_t gid)
{
	int res;
	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);
	DP("called %s. Path: %s\n",__func__,fullpath);	

	struct pkg_number *obj1 = init_pkg_number();
	obj1 -> type = 13;
	send_syscall_number( obj1);
	
	struct pkg_chown_send *obj2 = init_pkg_chown_send();
	strcpy(obj2->path,fullpath);
	obj2->uid = uid;
	obj2->gid = gid;
	send_syscall_chown_send(obj2);
	DP("sent to server\n");

	struct pkg_chown_receive *obj3 = init_pkg_chown_receive();
	receive_syscall_chown_receive(obj3);
	DP("received from server\n");
	res = obj3->res;
	free(obj1); free(obj2); free(obj3);
	if (res < 0){
		DP("%s failed\n\n\n",__func__);	
		return res;
	}
	DP("%s done\n\n\n",__func__);	
	return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
	int res;
	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);
	DP("called %s. Path: %s\n",__func__,fullpath);	

	struct pkg_number *obj1 = init_pkg_number();
	obj1 -> type = 14;
	send_syscall_number( obj1);
	
	struct pkg_open_send *obj2 = init_pkg_open_send();
	strcpy(obj2->path,fullpath);
	obj2->fi = *fi;
	send_syscall_open_send(obj2);
	DP("sent to server\n");

	struct pkg_open_receive *obj3 = init_pkg_open_receive();
	receive_syscall_open_receive(obj3);
	DP("received from server\n");
	res = obj3->res;
	free(obj1); free(obj2); free(obj3);
	if (res < 0){
		DP("%s failed\n\n\n",__func__);	
		return res;
	}
	DP("%s done\n\n\n",__func__);	
	return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
	int fd;
	int res;
	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);
	DP("called %s. Path: %s\n",__func__,fullpath);	
	(void) fi;
	struct pkg_number *obj1 = init_pkg_number();
	obj1 -> type = 15 ; 
	send_syscall_number( obj1); 
	struct pkg_read_send *obj2 = init_pkg_read_send(); 
	strcpy(obj2->path,fullpath); 
	//strcpy(obj2->buf, buf); 
	obj2->size = size; 
	obj2->offset = offset; 
	send_syscall_read_send(obj2); 
	DP("sent to server. Size is %ld XXXX\n",size);

	struct pkg_read_receive *obj3 = init_pkg_read_receive();
	receive_syscall_read_receive(obj3);
	DP("received from server\n");

	res = obj3->res; 
	free(obj1); free(obj2); 
	if (res < 0){ 
		free(obj3); 
		DP("%s failed\n\n\n",__func__);	
		return res; 
	} 
	//disregarding convention here
	readAll(sockfd1, buf, size);
	//strcpy(buf,obj3->buf); 
	DP("%s done\n\n\n",__func__);	
	return res; 
} 

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
	int fd;
	int res;
	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);
	DP("called %s. Path: %s\n",__func__,fullpath);	
	(void) fi;
	struct pkg_number *obj1 = init_pkg_number();
	obj1 -> type = 16 ; 
	send_syscall_number( obj1); 
	struct pkg_write_send *obj2 = init_pkg_write_send(); 
	strcpy(obj2->path,fullpath); 
	strcpy(obj2->buf, buf); 
	obj2->size = size; 
	obj2->offset = offset; 
	send_syscall_write_send(obj2); 
	DP("sent to server. Size is %ld XXXX\n",size);

	struct pkg_write_receive *obj3 = init_pkg_write_receive();
	receive_syscall_write_receive(obj3);
	DP("received from server\n");

	res = obj3->res; 
	free(obj1); free(obj2); 
	if (res < 0){ 
		free(obj3); 
		DP("%s failed\n\n\n",__func__);	
		return res; 
	} 
	//strcpy(buf,obj3->buf); 
	DP("%s done\n\n\n",__func__);	
	return res; 
} 

static int xmp_truncate(const char *path, off_t size)
{
	int res;
	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);
	DP("called %s. Path: %s\n",__func__,fullpath);	

	struct pkg_number *obj1 = init_pkg_number();
	obj1 -> type = 17;
	send_syscall_number( obj1);
	
	struct pkg_truncate_send *obj2 = init_pkg_truncate_send();
	strcpy(obj2->path,fullpath);
	obj2->size = size;
	send_syscall_truncate_send(obj2);
	DP("sent to server\n");

	struct pkg_truncate_receive *obj3 = init_pkg_truncate_receive();
	receive_syscall_truncate_receive(obj3);
	DP("received from server\n"); 
	res = obj3->res;
	free(obj1); free(obj2); free(obj3);
	if (res == -1){
		DP("%s failed\n\n\n",__func__); 
		return -errno;
	}
	DP("%s done\n\n\n",__func__); 
	return 0;
}

//#ifdef HAVE_UTIMENSAT
static int xmp_utimens(const char *path, const struct timespec ts[2])
{
	int res;
	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);
	DP("called %s. Path: %s\n",__func__,fullpath);

	struct pkg_number *obj1 = init_pkg_number();
	obj1 -> type = 18;
	send_syscall_number( obj1);
	
	struct pkg_utimens_send *obj2 = init_pkg_utimens_send();
	strcpy(obj2->path,fullpath);
	obj2->ts[0] = ts[0];
	obj2->ts[1] = ts[1];
	send_syscall_utimens_send(obj2);
	DP("sent to server\n");

	struct pkg_utimens_receive *obj3 = init_pkg_utimens_receive();
	receive_syscall_utimens_receive(obj3);
	DP("received from server\n"); 
	res = obj3->res;
	free(obj1); free(obj2); free(obj3);

	/* don't use utime/utimes since they follow symlinks */
	if (res == -1){
		DP("%s failed\n\n\n",__func__);
		return -errno;
	}
	DP("%s done\n\n\n",__func__);
	return 0;
}
//#endif

static int xmp_statfs(const char *path, struct statvfs *stbuf)
{
	int res;
	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);
	DP("called %s. Path: %s\n",__func__,fullpath);

	struct pkg_number *obj1 = init_pkg_number();
	obj1 -> type = 19;
	send_syscall_number( obj1);
	
	struct pkg_statfs_send *obj2 = init_pkg_statfs_send();
	strcpy(obj2->path,fullpath);
	printf("statfs path sending to server = %s\n", obj2->path);
	memcpy(&obj2->stbuf, stbuf , sizeof(struct statvfs));
	send_syscall_statfs_send(obj2);
	DP("sent to server\n"); 

	struct pkg_statfs_receive *obj3 = init_pkg_statfs_receive();
	receive_syscall_statfs_receive(obj3);
	DP("received from server\n"); 
	res = obj3->res;
	free(obj1); free(obj2); free(obj3);
	
	if (res < 0){
		DP("%s failed\n\n\n",__func__); 
		return res;
	}
	DP("%s done\n\n\n",__func__); 
	return 0;
}

static int xmp_release(const char *path, struct fuse_file_info *fi)
{
	/* Just a stub.	 This method is optional and can safely be left
	   unimplemented */

	(void) path;
	(void) fi;
	return 0;
}

static int xmp_fsync(const char *path, int isdatasync,
		     struct fuse_file_info *fi)
{
	/* Just a stub.	 This method is optional and can safely be left
	   unimplemented */

	printf("%s():%d %s\n",__func__,__LINE__,path);	
	(void) path;
	(void) isdatasync;
	(void) fi;
	return 0;
}

#ifdef HAVE_POSIX_FALLOCATE
static int xmp_fallocate(const char *path, int mode,
			off_t offset, off_t length, struct fuse_file_info *fi)
{
	int fd;
	int res;
	char fullpath[PATH_MAX];
	create_fullpath(fullpath, path);
	DP("called %s. Path: %s\n",__func__,fullpath);

	(void) fi;

	if (mode)
		return -EOPNOTSUPP;

	struct pkg_number *obj1 = init_pkg_number();
	obj1 -> type = 20;
	send_syscall_number( obj1);
	
	struct pkg_fallocate_send *obj2 = init_pkg_fallocate_send();
	strcpy(obj2->path,fullpath);
	obj2->offset = offset;
	obj2->length = length;
	send_syscall_statfs_send(obj2);
	DP("sent to server\n"); 

	struct pkg_fallocate_receive *obj3 = init_pkg_fallocate_receive();
	receive_syscall_fallocate_receive(obj3);
	DP("received from server\n"); 
	res = obj3->res;
	free(obj1); free(obj2); free(obj3);
	return res;
}
#endif

static struct fuse_operations xmp_oper = {
	.getattr	= xmp_getattr,
	.access		= xmp_access,
	.readlink	= xmp_readlink,
	.readdir	= xmp_readdir,
	.mknod		= xmp_mknod,
	.mkdir		= xmp_mkdir,
	.symlink	= xmp_symlink,
	.unlink		= xmp_unlink,
	.rmdir		= xmp_rmdir,
	.rename		= xmp_rename,
	.link		= xmp_link,
	.chmod		= xmp_chmod,
	.chown		= xmp_chown,
	.truncate	= xmp_truncate,
//#ifdef HAVE_UTIMENSAT
	.utimens	= xmp_utimens,
//#endif
	.open		= xmp_open,
	.read		= xmp_read,
	.write		= xmp_write,
	.statfs		= xmp_statfs,
	.release	= xmp_release,
	.fsync		= xmp_fsync,
#ifdef HAVE_POSIX_FALLOCATE
	.fallocate	= xmp_fallocate,
#endif
};

int main(int argc, char *argv[])
{
	umask(0);

	if(argc !=3) {
		printf("Usage ./client [mountPoint] [ServerName]");
	}

	/* 1. Make connection with the server here do not initiate send and receive
	 * 2. Send and receive takes place inside the functions separately.
	 * 3. Send sends the data to the server and blocks to receive the data from the Server
	 * 4. The received data is accepted and function returns in normal fashion.
	 * */

	// 1. Making Connection 
	int numbytes;
	socklen_t addr_len;
    int sockfd;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    struct in_addr ipv4addr;

    char buffer[256];

	//inet_pton(AF_INET, "10.193.4.23", &ipv4addr);
	//server = gethostbyaddr(&ipv4addr, sizeof ipv4addr, AF_INET);

	//if(inet_pton(AF_INET, "10.193.4.23", &serv_addr.sin_addr)<=0)
    //{
    //    printf("\n inet_pton error occured\n");
    //    return 1;
    //} 


	//inet_aton("10.193.4.23", &ipv4addr);
	//server = gethostbyaddr(&ipv4addr, sizeof ipv4addr, AF_INET);

    //server = gethostbyname("shehbaz");


	//sockfd1 = startClient( 5000, "10.17.5.73"); //Magrathea
	sockfd1 = startClient( 5000, "127.0.0.1");
	printf("Client :: All set, starting fusemain\n");
	return fuse_main(argc, argv, &xmp_oper, NULL);
}
